
# -d @$PWD/R/Ref.JSON \
# -d "delta"="0.3" \
# -d "query"='{"peaklists": [{"ppm": 3.14, "i": 0.0},{"ppm": 2.98, "i": 0.0}]}' \

curl -X POST \
   -d "delta"="0.02" \
   -d "matchingMethod"="all" \
   -d "query"='{"peaklists": [{"ppm": 1.255, "i": 0},{"ppm": 1.272, "i": 0},{"ppm": 1.287, "i": 0},{"ppm": 1.306, "i": 0},{"ppm": 1.321, "i": 0}]}' \
http://127.0.0.1:8080/PM/webservice

