FROM ubuntu:14.04

RUN apt-get update \
    && apt-get install -y \
    sudo \
    wget \
    curl \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    software-properties-common

RUN add-apt-repository ppa:opencpu/rapache \
    && apt-get update \
    && apt-get install -y libapache2-mod-r-base \
    && wget http://cran.r-project.org/src/contrib/bitops_1.0-6.tar.gz \
    && R CMD INSTALL bitops_1.0-6.tar.gz \
    && wget http://cran.r-project.org/src/contrib/RCurl_1.95-4.6.tar.gz \
    && R CMD INSTALL RCurl_1.95-4.6.tar.gz \
    && wget http://cran.r-project.org/src/contrib/rjson_0.2.15.tar.gz \
    && R CMD INSTALL rjson_0.2.15.tar.gz

# Set Apache environment variables (can be changed on docker run with -e)
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_SERVERADMIN admin@localhost
ENV APACHE_SERVERNAME localhost
ENV APACHE_SERVERALIAS docker.localhost
ENV APACHE_DOCUMENTROOT /var/www

COPY my-site.conf /etc/apache2/sites-enabled/001-my-site.conf

COPY apache2-foreground /usr/local/bin/

RUN mkdir /var/www/html/tmp \
    && chmod 777 /var/www/html/tmp \
    && chmod 755 /usr/local/bin/apache2-foreground

EXPOSE 80

CMD ["/usr/local/bin/apache2-foreground"]
