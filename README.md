# PeakMatching Webservice V1.0 #
29/06/2015

- - -

## Description ##
Web-service and web-application to access and process NMR peakMatching.

### docker-Rapache/ ###
Dockerfile, my-site.conf, run --------------> to build and run Docker  
Request.sh ---------------------------------> example of curl request

### R/ ###
 Ref.JSON -----------------------------------> an internal library



## Installation ##

### Install Docker ###
See https://docs.docker.com/installation/

### Get the latest ubuntu ###

```
#!bash

$ sudo docker pull ubuntu:latest
```


### Unzip docker-Rapache.zip ###

```
#!bash

$ unzip docker-Rapache.zip -d yourWorkdir
```


### In yourWorkdir/docker-Rapache, build docker image of Rapache ###

```
#!sh

$ sudo sh ./run build
```

## Usage ##

### To watch ###

```
#!bash

$ sudo docker images
```


### Run rapache container & redirect port ###

```
#!bash

$ sudo sh ./run start
```


### To stop rapache container ###

```
#!bash

$ sudo sh ./run stop
```


### To view the rapache container status ###

```
#!bash

$ sudo sh ./run status
```


Check on http://127.0.0.1:8080/PM/webapp  
Perform request.sh